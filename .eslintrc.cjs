/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution');

module.exports = {
  root: true,
  extends: [
    // https://github.com/antfu/eslint-config
    '@antfu',
    'plugin:prettier-vue/recommended',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',

    // Enforce semi
    semi: ['error', 'always'],
    '@typescript-eslint/semi': ['error', 'always'],
    '@typescript-eslint/member-delimiter-style': 'off',

    'vue/singleline-html-element-content-newline': 'off',

    'antfu/if-newline': 'off',

    'prettier-vue/prettier': [
      'error',
      {
        // Override all options of `prettier` here
        // @see https://prettier.io/docs/en/options.html
        semi: true,
        tralingComma: 'all',
        singleQuote: true,
        printWidth: 80,
        htmlWhitespaceSensitivity: 'ignore',
      },
    ],
  },
};
