import bg from './bg.json';
import et from './et.json';
import lt from './lt.json';
import ro from './ro.json';
import cs from './cs.json';
import fi from './fi.json';
import lv from './lv.json';
import sk from './sk.json';
import da from './da.json';
import fr from './fr.json';
import mt from './mt.json';
import sl from './sl.json';
import de from './de.json';
import ga from './ga.json';
import nl from './nl.json';
import sv from './sv.json';
import el from './el.json';
import hr from './hr.json';
import no from './no.json';
import en from './en.json';
import hu from './hu.json';
import pl from './pl.json';
import es from './es.json';
import it from './it.json';
import pt from './pt.json';

export const messages = {
  bg,
  et,
  lt,
  ro,
  cs,
  fi,
  lv,
  sk,
  da,
  fr,
  mt,
  sl,
  de,
  ga,
  nl,
  sv,
  el,
  hr,
  no,
  en,
  hu,
  pl,
  es,
  it,
  pt,
};
