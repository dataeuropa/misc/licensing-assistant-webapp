import type { MaybeRef } from 'vue';
import { computed, ref } from 'vue';
import type {
  License,
  LicenseTermTitle,
  LicenseTermType,
} from '../utils/schema/license';

/**
 * Returns reactive state and handlers related to license filters.
 */
export function useLicenseFilters(_licenses: MaybeRef<License[]>) {
  const licenses = toRef(_licenses);

  /**
   * Selected obligation terms
   */
  const selectedObligations = ref<LicenseTermTitle[]>([]);

  /**
   * Selected permission terms
   */
  const selectedPermissions = ref<LicenseTermTitle[]>([]);

  /**
   * Selected prohibition terms
   */
  const selectedProhibitions = ref<LicenseTermTitle[]>([]);

  const selectedLicenseFilters = computed(() => {
    return {
      Obligation: selectedObligations.value,
      Permission: selectedPermissions.value,
      Prohibition: selectedProhibitions.value,
    };
  });

  /**
   * Removes a term from a specific license filter category
   */
  const handleRemove = ({
    key: licenseType,
    value: licenseTitle,
  }: {
    key: LicenseTermType;
    value: LicenseTermTitle;
  }) => {
    const targetLicenseType =
      licenseType === 'Obligation'
        ? selectedObligations
        : licenseType === 'Permission'
        ? selectedPermissions
        : selectedProhibitions;

    const index = targetLicenseType.value.indexOf(licenseTitle);
    if (index > -1) {
      targetLicenseType.value.splice(index, 1);
    }
  };

  /**
   * Clears all selected license filters.
   */
  const handleClearFilters = () => {
    selectedObligations.value.length = 0;
    selectedPermissions.value.length = 0;
    selectedProhibitions.value.length = 0;
  };

  /**
   * Toggles weighted filtering.
   */
  const weightedFiltering = ref(false);

  /**
   * Returns computed license rows based on selected license filters.
   * Applies weighted filtering if enabled.
   */
  const computedLicenseRows = computed(() => {
    let filteredLicenses = licenses.value.map((license) => {
      let weight = 0;
      const isMatch = Object.entries(selectedLicenseFilters.value).every(
        ([key, value]) => {
          if (value.length === 0) {
            return true;
          }

          const matches = value.every((criteria) => {
            const match = license.terms.some(
              (term) => term.type === key && term.title === criteria
            );
            if (match) {
              weight++;
            }
            return match;
          });

          return matches;
        }
      );

      return {
        ...license,
        weight: isMatch ? weight : 0,
        isMatch,
      };
    });

    if (weightedFiltering.value) {
      filteredLicenses.sort((a, b) => b.weight - a.weight);
    } else {
      filteredLicenses = filteredLicenses.filter((license) => license.isMatch);
    }

    return filteredLicenses.map((license) => {
      return {
        name: license.name,
        terms: license.terms.map((term) => {
          return `${term.type}: ${term.title}`;
        }),
      };
    });
  });

  return {
    selectedObligations,
    selectedPermissions,
    selectedProhibitions,
    selectedLicenseFilters,
    handleRemove,
    handleClearFilters,
    weightedFiltering,
    computedLicenseRows,
  };
}
