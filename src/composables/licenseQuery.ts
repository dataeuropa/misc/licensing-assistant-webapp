import { useQuery } from '@tanstack/vue-query'
import { getLicenses, getLicenseById } from '../utils/licensingAssistantApi'
import { MaybeRef } from 'vue'


export function useLicensesQuery() {
  return useQuery(['licenses'], getLicenses)
}

export function useLicenseQuery(id: MaybeRef<string>) {
  return useQuery(['license', id], async () => {
    
    const res =  await getLicenseById(unref(id))
    if (!res) {
      throw new Error('License not found')
    }

    return res
  })
}