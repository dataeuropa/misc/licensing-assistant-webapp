# Views

This folder contains views for the application. A view is a Vue component that is registered as a route in [router.ts](../plugins/router.ts).

View components should be named with the suffix `View` to distinguish them from other components.

View components have access
  - API related business logic
  - routing related logic
  - state management

Components that are not views should not have access to these things. Instead, they should be passed data and callbacks from their parent component.