import { createRouter, createWebHistory } from 'vue-router';
import { headLicenseById } from '../utils/licensingAssistantApi';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: { name: 'licenses' },
    },
    {
      path: '/licences',
      name: 'licenses',
      component: () => import('../views/LicensesView.vue'),
    },
    {
      path: '/licences/licence',
      name: 'license-details',
      component: () => import('../views/LicenseDetailsView.vue'),
      beforeEnter: async (to) => {
        // check if licenseId exists in backendd
        const license = await headLicenseById(to.query.id as string);
        if (!license) {
          return { name: 'not-found' };
        }
        return true;
      },
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'not-found',
      component: () => import('../views/NotFoundView.vue'),
    },
  ],
});

export default router;
