import type { App } from 'vue';

import { VueQueryPlugin } from '@tanstack/vue-query';
import router from './router';

export function registerPlugins(app: App) {
  app.use(router);
  app.use(VueQueryPlugin);
}
