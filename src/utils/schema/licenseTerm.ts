import * as z from 'zod';

// Extracted from
// https://gitlab.fokus.fraunhofer.de/dataeuropa/misc/licensing-assistant/-/blob/3e26f5b2cd637c550268e4c99e5ca2c5663a9e6e/src/main/resources/ontology/copyright.owl
const licenseTermTitleSchema = z.union([
  z.literal('LesserCopyleft'),
  z.literal('Reproduction'),
  z.literal('DerivativeWorks'),
  z.literal('Distribution'),
  z.literal('Attribution'),
  z.literal('ShareAlike'),
  z.literal('Notice'),
  z.literal('Sublicensing'),
  z.literal('Copyleft'),
  z.literal('StateChanges'),
  z.literal('CommercialUse'),
  z.literal('PatentGrant'),
])

// Extracted from
// https://gitlab.fokus.fraunhofer.de/dataeuropa/misc/licensing-assistant/-/blob/3e26f5b2cd637c550268e4c99e5ca2c5663a9e6e/src/main/resources/ontology/copyright.owl
const licenseTermTypeSchema = z.union([
  z.literal('Obligation'),
  z.literal('Permission'),
  z.literal('Prohibition'),
])

const licenseTermSchema = z.object({
  id: z.string(),
  title: licenseTermTitleSchema,
  type: licenseTermTypeSchema,
  description: z.string().optional(),
})

export { licenseTermSchema, licenseTermTitleSchema, licenseTermTypeSchema };
export type LicenseTerm = z.infer<typeof licenseTermSchema>;
export type LicenseTermTitle = z.infer<typeof licenseTermTitleSchema>;
export type LicenseTermType = z.infer<typeof licenseTermTypeSchema>;
