import * as z from 'zod';
import { licenseTermSchema } from './licenseTerm';

const licenseSchema = z.object({
  id: z.string(),
  name: z.string(),
  shortName: z.string(),
  longName: z.string().optional(),
  description: z.string().nullable().optional(),
  version: z.string().nullable().optional(),
  url: z.string().url().nullable().optional(),
  terms: z.array(licenseTermSchema),
  compatibleLicenses: z.array(z.string()),
});

export * from './licenseTerm';

export { licenseSchema };
export type License = z.infer<typeof licenseSchema>;
