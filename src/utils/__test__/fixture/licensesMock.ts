export default [
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-SA3.0NL",
    "name": "CC-BY-SA 3.0 NL",
    "shortName": "CC-BY-SA3.0NL",
    "terms": [
      {
        "id": "LesserCopyleft",
        "title": "LesserCopyleft",
        "type": "Obligation",
        "description": "derivative works must be licensed under specified terms, with at least the same conditions as the original work; combinations with the work may be licensed under different terms"
      },
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by-sa/3.0/nl/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#ODC-PDDL",
    "name": "ODC-PDDL",
    "shortName": "ODC-PDDL",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CC-PDM1.0",
      "CC01.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "DL-DE-ZERO2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "ODC-PDDL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://opendatacommons.org/licenses/pddl/1-0/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#ODC-ODbL",
    "name": "ODC-ODbL",
    "shortName": "ODC-ODbL",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "ODC-BY",
      "ODC-ODbL"
    ],
    "description": null,
    "version": "???",
    "url": "http://opendatacommons.org/licenses/odbl/1.0/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#DL-DE-BY-NC1.0",
    "name": "DL-DE-BY-NC 1.0",
    "shortName": "DL-DE-BY-NC1.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "CommercialUse",
        "title": "CommercialUse",
        "type": "Prohibition",
        "description": "exercising rights for commercial purposes"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "DL-DE-BY-NC1.0"
    ],
    "description": null,
    "version": "???",
    "url": "https://www.govdata.de/dl-de/by-nc-1-0"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#DL-DE-BY2.0",
    "name": "DL-DE-BY 2.0",
    "shortName": "DL-DE-BY2.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA4.0",
      "CC-BY4.0",
      "DL-DE-BY2.0",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3"
    ],
    "description": null,
    "version": "???",
    "url": "https://www.govdata.de/dl-de/by-2-0"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#DL-DE-BY1.0",
    "name": "DL-DE-BY 1.0",
    "shortName": "DL-DE-BY1.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA4.0",
      "CC-BY4.0",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3"
    ],
    "description": null,
    "version": "???",
    "url": "https://www.govdata.de/dl-de/by-1-0"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-SA4.0",
    "name": "CC-BY-SA  4.0",
    "shortName": "CC-BY-SA4.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-SA4.0"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by-sa/4.0/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY4.0",
    "name": "CC-BY  4.0",
    "shortName": "CC-BY4.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by/4.0/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-NC-ND4.0",
    "name": "CC-BY-NC-ND  4.0",
    "shortName": "CC-BY-NC-ND4.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "CommercialUse",
        "title": "CommercialUse",
        "type": "Prohibition",
        "description": "exercising rights for commercial purposes"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by-nc-nd/4.0/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY3.0NL",
    "name": "CC-BY 3.0 NL",
    "shortName": "CC-BY3.0NL",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by/3.0/nl/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#OGL2.0",
    "name": "OGL 2.0",
    "shortName": "OGL2.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/2/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#ODC-BY",
    "name": "ODC-BY",
    "shortName": "ODC-BY",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "ODC-BY"
    ],
    "description": null,
    "version": "???",
    "url": "http://opendatacommons.org/licenses/by/1.0/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#OGL3.0",
    "name": "OGL 3.0",
    "shortName": "OGL3.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#IODLv1.0",
    "name": "IODL v1.0",
    "shortName": "IODLv1.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "IODLv1.0",
      "ODC-ODbL"
    ],
    "description": null,
    "version": "???",
    "url": "http://www.formez.it/iodl/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#IODLv2.0",
    "name": "IODL v2.0",
    "shortName": "IODLv2.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://www.dati.gov.it/iodl/2.0/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-NC4.0",
    "name": "CC-BY-NC  4.0",
    "shortName": "CC-BY-NC4.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "CommercialUse",
        "title": "CommercialUse",
        "type": "Prohibition",
        "description": "exercising rights for commercial purposes"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "DL-DE-BY-NC1.0",
      "OGL-NC",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by-nc/4.0/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#HR-OD",
    "name": "HR-OD",
    "shortName": "HR-OD",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA4.0",
      "CC-BY4.0",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "HR-OD"
    ],
    "description": null,
    "version": "???",
    "url": "http://data.gov.hr/open-licence-republic-croatia"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#FR-LO",
    "name": "FR-LO",
    "shortName": "FR-LO",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "PatentGrant",
        "title": "PatentGrant",
        "type": "Permission",
        "description": "granting a license to use any patents of the licensor applicable to the work insofar as needed to use the work in accordance with the copyright usage rights"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "https://www.etalab.gouv.fr/licence-ouverte-open-licence"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#GFDL-1.3",
    "name": "GFDL-1.3",
    "shortName": "GFDL-1.3",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "GFDL-1.3"
    ],
    "description": null,
    "version": "???",
    "url": "https://gnu.org/licenses/fdl-1.3.en.html"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CCBY3.0Austria",
    "name": "CC BY 3.0 Austria",
    "shortName": "CCBY3.0Austria",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "https://creativecommons.org/licenses/by/3.0/at/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#OGL1.0",
    "name": "OGL 1.0",
    "shortName": "OGL1.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/1/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#OGL-ROU-1.0",
    "name": "OGL-ROU-1.0",
    "shortName": "OGL-ROU-1.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://data.gov.ro/base/images/logoinst/OGL-ROU-1.0.pdf"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-PDM1.0",
    "name": "CC-PDM  1.0",
    "shortName": "CC-PDM1.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CC-PDM1.0",
      "CC01.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "DL-DE-ZERO2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "ODC-PDDL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/publicdomain/mark/1.0/"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#OGL-NC",
    "name": "OGL-NC",
    "shortName": "OGL-NC",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "CommercialUse",
        "title": "CommercialUse",
        "type": "Prohibition",
        "description": "exercising rights for commercial purposes"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "OGL-NC",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://www.nationalarchives.gov.uk/doc/non-commercial-government-licence/non-commercial-government-licence.htm"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-NC-SA4.0",
    "name": "CC-BY-NC-SA 4.0",
    "shortName": "CC-BY-NC-SA4.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "CommercialUse",
        "title": "CommercialUse",
        "type": "Prohibition",
        "description": "exercising rights for commercial purposes"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-SA4.0"
    ],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#NLOD",
    "name": "NLOD",
    "shortName": "NLOD",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CCBY3.0Austria",
      "DL-DE-BY2.0",
      "EUPL-1.1",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "OGL-NC",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "http://data.norge.no/nlod/en/1.0"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#PSEUL",
    "name": "PSEUL",
    "shortName": "PSEUL",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "CommercialUse",
        "title": "CommercialUse",
        "type": "Prohibition",
        "description": "exercising rights for commercial purposes"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [],
    "description": null,
    "version": "???",
    "url": "http://www.ordnancesurvey.co.uk/business-and-government/public-sector/mapping-agreements/inspire-licence.html"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC01.0",
    "name": "CC0 1.0",
    "shortName": "CC01.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CC-PDM1.0",
      "CC01.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "DL-DE-ZERO2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "ODC-PDDL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "https://creativecommons.org/publicdomain/zero/1.0/legalcode"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#EUPL-1.1",
    "name": "EUPL-1.1",
    "shortName": "EUPL-1.1",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "PatentGrant",
        "title": "PatentGrant",
        "type": "Permission",
        "description": "granting a license to use any patents of the licensor applicable to the work insofar as needed to use the work in accordance with the copyright usage rights"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "EUPL-1.1",
      "EUPL-1.2"
    ],
    "description": null,
    "version": "???",
    "url": "https://joinup.ec.europa.eu/software/page/eupl/licence-eupl"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#GFDL-1.2",
    "name": "GFDL-1.2",
    "shortName": "GFDL-1.2",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "GFDL-1.2"
    ],
    "description": null,
    "version": "???",
    "url": "https://gnu.org/licenses/old-licenses/fdl-1.2"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#DL-DE-ZERO2.0",
    "name": "DL-DE-ZERO 2.0",
    "shortName": "DL-DE-ZERO2.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      }
    ],
    "compatibleLicenses": [
      "CC-BY-NC-ND4.0",
      "CC-BY-NC-SA4.0",
      "CC-BY-NC4.0",
      "CC-BY-ND4.0",
      "CC-BY-SA3.0NL",
      "CC-BY-SA4.0",
      "CC-BY3.0NL",
      "CC-BY4.0",
      "CC-PDM1.0",
      "CC01.0",
      "CCBY3.0Austria",
      "DL-DE-BY-NC1.0",
      "DL-DE-BY1.0",
      "DL-DE-BY2.0",
      "DL-DE-ZERO2.0",
      "EUPL-1.1",
      "FR-LO",
      "GFDL-1.1",
      "GFDL-1.2",
      "GFDL-1.3",
      "IODLv1.0",
      "IODLv2.0",
      "NLOD",
      "ODC-BY",
      "ODC-ODbL",
      "ODC-PDDL",
      "OGL-NC",
      "OGL-ROU-1.0",
      "OGL1.0",
      "OGL2.0",
      "OGL3.0",
      "PSEUL"
    ],
    "description": null,
    "version": "???",
    "url": "https://www.govdata.de/dl-de/zero-2-0"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#GFDL-1.1",
    "name": "GFDL-1.1",
    "shortName": "GFDL-1.1",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "GFDL-1.1"
    ],
    "description": null,
    "version": "???",
    "url": "https://gnu.org/licenses/old-licenses/fdl-1.1"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#EUPL-1.2",
    "name": "EUPL-1.2",
    "shortName": "EUPL-1.2",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "Copyleft",
        "title": "Copyleft",
        "type": "Obligation",
        "description": "derivative and combined works must be licensed under specified terms, similar to those on the original work"
      },
      {
        "id": "PatentGrant",
        "title": "PatentGrant",
        "type": "Permission",
        "description": "granting a license to use any patents of the licensor applicable to the work insofar as needed to use the work in accordance with the copyright usage rights"
      },
      {
        "id": "DerivativeWorks",
        "title": "DerivativeWorks",
        "type": "Permission",
        "description": "distribution of derivative works"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Sublicensing",
        "title": "Sublicensing",
        "type": "Permission",
        "description": "granting a license yourself to a new licensee under the same terms as the original license"
      },
      {
        "id": "ShareAlike",
        "title": "ShareAlike",
        "type": "Obligation",
        "description": "derivative works be licensed under the same terms or compatible terms as the original work"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [
      "EUPL-1.1",
      "EUPL-1.2"
    ],
    "description": null,
    "version": "???",
    "url": "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863"
  },
  {
    "id": "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-ND4.0",
    "name": "CC-BY-ND 4.0",
    "shortName": "CC-BY-ND4.0",
    "terms": [
      {
        "id": "Reproduction",
        "title": "Reproduction",
        "type": "Permission",
        "description": "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium."
      },
      {
        "id": "StateChanges",
        "title": "StateChanges",
        "type": "Obligation",
        "description": "derivative or changed works (i.e. including partial distributions/reproductions of works, as permitted e.g. under CC-BY-ND) must indicate which changes have been made to the original licensed work in a manner that permits attribution"
      },
      {
        "id": "Distribution",
        "title": "Distribution",
        "type": "Permission",
        "description": "distribution, public display, and publicly performance"
      },
      {
        "id": "Attribution",
        "title": "Attribution",
        "type": "Obligation",
        "description": "credit be given to copyright holder and/or author"
      },
      {
        "id": "Notice",
        "title": "Notice",
        "type": "Obligation",
        "description": "copyright and license notices be kept intact"
      }
    ],
    "compatibleLicenses": [],
    "description": null,
    "version": "???",
    "url": "http://creativecommons.org/licenses/by-nd/4.0/legalcode"
  }
]
