import { describe, expect, it } from 'vitest';
import * as z from 'zod';
import licensesMock from './fixture/licensesMock';
import { licenseSchema } from '../schema/license';

describe('License schema', () => {

  it('validates correctly', () => {
    const parsed = z.array(licenseSchema).safeParse(licensesMock);
    if (!parsed.success) {
      console.log(parsed.error.issues[0].path);
    }
    expect(parsed.success).toBe(true);
  });
});

