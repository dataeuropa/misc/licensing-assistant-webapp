import Axios from 'axios';
import * as z from 'zod';
import { licenseSchema } from './schema/license';

const api = Axios.create({
  baseURL: import.meta.env.VITE_API_URL,
});

export async function getLicenses() {
  const response = await api.get('/licenses');
  return z.array(licenseSchema).parse(response.data);
}

export async function getLicenseById(id: string) {
  const response = await api.get(`/licenses/${id}`);

  // If 204 No Content, return null
  if (response.status === 204) {
    return null;
  }

  return licenseSchema.parse(response.data);
}

export async function headLicenseById(id: string) {
  const response = await api.head(`/licenses/${id}`);
  return response.status === 200;
}
