import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import { nextTick } from 'vue';
import EcSelect from '../common/EcSelect.vue';

describe('EcSelect', () => {
  it('Updates v-model when checkbox selected', async () => {
    const options = [
      { label: 'Option 1', id: 'option1' },
      { label: 'Option 2', id: 'option2' },
    ];
    const wrapper = mount(EcSelect, {
      global: {
        renderStubDefaultSlot: true,
      },
      shallow: true,
      props: {
        name: 'foobar',
        options,
        'onUpdate:modelValue': (e: string[]) =>
          wrapper.setProps({ modelValue: e }),
      },
    });
    // Simulate user interaction by selecting a checkbox
    await wrapper.find('#select-multiple--foobar-option1').setValue();
    await nextTick();

    // Check if v-model gets updated correctly
    expect(wrapper.props('modelValue')).toEqual(['option1']);
  });

  it('Updates v-model when "select all" checkbox selected', async () => {
    const options = [
      { label: 'Option 1', id: 'option1' },
      { label: 'Option 2', id: 'option2' },
    ];
    const wrapper = mount(EcSelect, {
      global: {
        renderStubDefaultSlot: true,
      },
      shallow: true,
      props: {
        name: 'foobar',
        options,
        modelValue: [],
        'onUpdate:modelValue': (e: string[]) =>
          wrapper.setProps({ modelValue: e }),
      },
    });

    // Simulate user interaction by selecting "select all" checkbox
    await wrapper.find('#select-multiple-all--foobar').setValue();
    await nextTick();

    // Check if v-model gets updated correctly
    expect(wrapper.props('modelValue')).toEqual(['option1', 'option2']);
  });

  it('Updates v-model when "clear all" button clicked', async () => {
    const options = [
      { label: 'Option 1', id: 'option1' },
      { label: 'Option 2', id: 'option2' },
    ];
    const wrapper = mount(EcSelect, {
      global: {
        renderStubDefaultSlot: true,
      },
      shallow: true,
      props: {
        name: 'foobar',
        options,
        modelValue: ['option1', 'option2'],
        'onUpdate:modelValue': (e: string[]) => {
          wrapper.setProps({ modelValue: e });
        },
      },
    });

    // Simulate user interaction by clicking "clear all" button
    await wrapper.find('button.ecl-button--ghost').trigger('click');
    await nextTick();

    // Check if v-model gets updated correctly
    expect(wrapper.props('modelValue')).toEqual([]);
  });

  it('updates the selected counter when selecting items', async () => {
    const options = [
      { label: 'Option 1', id: 'option1' },
      { label: 'Option 2', id: 'option2' },
      { label: 'Option 3', id: 'option3' },
      { label: 'Option 4', id: 'option4' },
    ];
    const wrapper = mount(EcSelect, {
      global: {
        renderStubDefaultSlot: true,
      },
      shallow: true,
      props: {
        name: 'foobar',
        options,
        modelValue: ['option1', 'option2'],
        'onUpdate:modelValue': (e: string[]) => {
          wrapper.setProps({ modelValue: e });
        },
      },
    });

    // Simulate user interaction by clicking "clear all" button
    const counter = await wrapper.find(
      '.ecl-select-multiple-selections-counter'
    );

    expect(counter.text()).toBe('2');

    // Toggle option 1
    await wrapper.find('#select-multiple--foobar-option1').setValue(false);
    await nextTick();

    expect(counter.text()).toBe('1');

    // Toggle all
    await wrapper.find('#select-multiple-all--foobar').setValue();
    await nextTick();

    expect(counter.text()).toBe(options.length.toString());

    // Clear all
    await wrapper.find('button.ecl-button--ghost').trigger('click');
    // Element should not exist
    expect(
      wrapper.find('.ecl-select-multiple-selections-counter').exists()
    ).toBe(false);
  });
});
