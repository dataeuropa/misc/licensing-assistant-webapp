import { createApp } from 'vue';
import './style.scss';
import App from './App.vue';

// import { Header, Footer } from '@deu/deu-header-footer';
import { registerPlugins } from './plugins';

const app = createApp(App);

// require('@deu/deu-header-footer/dist/deu-header-footer.css');

// app.component('deu-header', Header);
// app.component('deu-footer', Footer);

registerPlugins(app);

app.mount('#app');
